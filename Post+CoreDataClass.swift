//
//  Post+CoreDataClass.swift
//  LenvendoTest
//
//  Created by Admin on 20.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData
import UIKit

public class Post: NSManagedObject {
    
    static let currentContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    func changePost(withText text: String, image: UIImage?) {
        
        guard let image = image, let imageData = UIImageJPEGRepresentation(image, 0.3) else { return }
        self.text = text
        self.image = imageData as NSData
        self.date = Date() as NSDate?
        Post.attemptToSaveInContext()
    }
    
    static func addPost(withText text: String, image: UIImage?, minutesAgo: Double, isPosted: Bool = false) {
        
        guard let image = image, let imageData = UIImageJPEGRepresentation(image, 0.3) else { return }
        let post = NSEntityDescription.insertNewObject(forEntityName: "Post", into: currentContext) as! Post
        post.text = text
        post.image = imageData as NSData
        post.date = Date().addingTimeInterval(-minutesAgo * 60) as NSDate?
        post.isPosted = isPosted
        
        Post.attemptToSaveInContext()
    }
    
    func setPosted() {
        self.isPosted = true
        
        Post.attemptToSaveInContext()

    }
    
    static func attemptToSaveInContext() {
        do {
            try Post.currentContext.save()
            
        } catch let error {
            print(error)
        }
    }
    
}
