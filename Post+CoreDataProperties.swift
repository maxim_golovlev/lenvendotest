//
//  Post+CoreDataProperties.swift
//  LenvendoTest
//
//  Created by Admin on 20.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension Post {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Post> {
        return NSFetchRequest<Post>(entityName: "Post");
    }

    @NSManaged public var image: NSData?
    @NSManaged public var text: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var isPosted: Bool

}
