//
//  ViewController.swift
//  LenvendoTest
//
//  Created by Admin on 20.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class FeedController: UIViewController {

    let cellId = "cellId"
    
    var posts = [Post]()
    
    lazy var fetchViewController: NSFetchedResultsController<NSFetchRequestResult> = {
        let currentContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Post")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        let fetchController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: currentContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchController.delegate = self
        return fetchController
    }()
    
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.register(PostCell.self, forCellReuseIdentifier: self.cellId)
        tv.dataSource = self
        tv.delegate = self
        tv.backgroundColor = grayColor
        tv.contentInset = UIEdgeInsetsMake(8, 0, 0, 0)
        return tv
    }()
    
    let activityIndicator: CustomActivityIndicator = {
        
        let activityView = CustomActivityIndicator(frame: .zero)
        activityView.isHidden = true
        activityView.translatesAutoresizingMaskIntoConstraints = false
        return activityView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        do {
            try fetchViewController.performFetch()
            print(fetchViewController.sections![0].numberOfObjects)
        } catch let error {
            print(error)
        }
        
        guard let postsCount = fetchViewController.sections?[0].objects?.count else { return }
        if postsCount == 0 { return }
        let lastIndexPath = IndexPath(row: postsCount - 1, section: 0)
        
        DispatchQueue.main.async {
            self.tableView.scrollToRow(at: lastIndexPath, at: .top, animated: false)
        }
    }
    
    func setupViews() {
        
        view.backgroundColor = .white
        navigationItem.title = "Feed"
        
        view.addSubview(tableView)
        view.addConstraintsWithFormat("H:|[v0]|", views: tableView)
        view.addConstraintsWithFormat("V:|[v0]|", views: tableView)
        
        view.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(createPostHandler))
        
    }
    
    func createPostHandler() {
        navigationController?.pushViewController(DetailPostController(), animated: true)
    }
}

extension FeedController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchViewController.sections![0].objects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! PostCell
        cell.post = fetchViewController.object(at: indexPath) as? Post
        cell.delegate = self
        return cell
    }
}

extension FeedController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            print("Deleted")
            
            let currentContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let postToDelete = fetchViewController.object(at: indexPath)
            
            currentContext.delete(postToDelete as! NSManagedObject)
            
            do {
                try currentContext.save()
                
            } catch let error {
                print(error)
            }
            
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        let post = fetchViewController.object(at: indexPath) as! Post
        
        if let size = estimageTextSize(text: post.text) {
            let pulicButtonHeight:CGFloat = post.isPosted ? 0 : 40 + 8
            return 8 + 150 + 8 + size.height + 8 + pulicButtonHeight
        }
        
        return 200
    }
    
    func estimageTextSize(text: String?) -> CGRect? {
        
        guard let text = text else { return nil }
        
        let width = UIScreen.main.bounds.width
        
        let size = CGSize(width: width, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
        
        return estimatedFrame
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let postToChange = fetchViewController.object(at: indexPath) as? Post {

            if postToChange.isPosted == true { return }
            
            let vc = DetailPostController()
            vc.post = postToChange
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension FeedController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        if type == .insert {
            self.tableView.insertRows(at: [newIndexPath!], with: .automatic)
            
            DispatchQueue.main.async {
                self.tableView.scrollToRow(at: newIndexPath!, at: .top, animated: true)
            }
            
            
        } else if type == .update {
            
            if let indexPath = indexPath, let visibleIndexPaths = tableView.indexPathsForVisibleRows?.index(of: indexPath as IndexPath) {
                if visibleIndexPaths != NSNotFound {
                    tableView.reloadRows(at: [indexPath], with: .fade)
                }
            }
        }
    }
}

extension FeedController: PostCellDelegate {
    
    func publishOnFacebook(post: Post?, completion: @escaping (Bool) -> ()) {
        guard let post = post else { return }
        
        activityIndicator.isHidden = false
        
        ApiService.sharedService.share(post: post, fromViewController: self) { uploaded in
            
            DispatchQueue.main.async {
                self.activityIndicator.isHidden = true
            }
            
            completion(uploaded)
        }
    }
}
