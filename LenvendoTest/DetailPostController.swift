//
//  CreatePostController.swift
//  LenvendoTest
//
//  Created by Admin on 20.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class DetailPostController: UIViewController {
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "image_placeholder")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleAvatarViewTap)))
        return imageView
    }()
    
    let textView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.backgroundColor = .white
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.becomeFirstResponder()
        return textView
    }()
    
    var isPostEditing = false
    
    var post: Post? {
        didSet {
            
            textView.text = post?.text
            
            if let imageData = post?.image as? Data {
                imageView.image = UIImage(data: imageData)
            }
            
            isPostEditing = true
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = grayColor
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(handleDone))
        
        view.addSubview(imageView)
        view.addSubview(textView)
        view.addConstraintsWithFormat("H:[v0(150)]", views: imageView)
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: textView)
        view.addConstraintsWithFormat("V:|-72-[v0(150)]-8-[v1(200)]->=8-|", views: imageView, textView)
    }
    
    func handleAvatarViewTap() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    func handleDone() {
        
        isPostEditing ? changePost() : createPost()
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    func changePost() {
        _ = post?.changePost(withText: textView.text, image: imageView.image)
    
    }
    
    func createPost() {
        _ = Post.addPost(withText: textView.text, image: imageView.image, minutesAgo: 0)
    }

}

extension DetailPostController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var image:UIImage?
        
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            image = editedImage
        } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            image = originalImage
        }
        
        if image != nil {
            imageView.image = image
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleAvatarViewTap)))
        }
        
        dismiss(animated: true, completion: nil)
    }
}
