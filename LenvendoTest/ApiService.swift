//
//  ApiService.swift
//  LenvendoTest
//
//  Created by Admin on 20.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class ApiService: NSObject {
    
    static let sharedService = ApiService()
    
    func share(post: Post, fromViewController vc: UIViewController, completion: @escaping (Bool) -> ()) {
        
        guard let imageData = post.image as? Data, let image = UIImage(data: imageData), let title = post.text else {return}
        
        let activityVC = UIActivityViewController(activityItems: [MyStringItemSource(textToShare: title),
                                                                  MyImageItemSource(image: image)], applicationActivities: nil )
        vc.present(activityVC, animated: true)
        
        activityVC.completionWithItemsHandler = {(activityType, completed, returnedItems, error) in
            completion(completed)
        }
    }
}

class MyStringItemSource: NSObject, UIActivityItemSource {
    
    var textToShare: String!
    
    init(textToShare: String) {
        self.textToShare = textToShare
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType) -> Any? {
            return textToShare
    }
}

class MyImageItemSource: NSObject, UIActivityItemSource {
    
    var image: UIImage
    
    init(image: UIImage) {
        self.image = image
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType) -> Any? {
        return image
    }
}


