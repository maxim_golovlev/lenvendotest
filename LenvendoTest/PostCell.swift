//
//  PostCell.swift
//  LenvendoTest
//
//  Created by Admin on 20.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

protocol PostCellDelegate: class {
    func publishOnFacebook(post: Post?, completion: @escaping (Bool)->())
}

class PostCell: UITableViewCell {
    
    weak var delegate: PostCellDelegate?
    
    let postImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var publishButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Share", for: .normal)
        button.addTarget(self, action: #selector(publishHandler), for: .touchUpInside)
        button.layer.borderWidth = 1
        button.backgroundColor = grayColor
        button.tintColor = .black
        return button
    }()
    
    var buttonHeightContraint: NSLayoutConstraint?
    var buttonBottomConnstraint: NSLayoutConstraint?
    
    var post: Post? {
        didSet {
            label.text = post?.text
            
            if let imageData = post?.image as? Data {
                postImage.image = UIImage(data: imageData)
            }
            
            if let date = post?.date as? Date {
                timeLabel.text = date.stringFromDate()
            }
            if post?.isPosted == true {
                self.hideButton()
            }
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        showButton()
    }
    
    func setupViews() {
        
        backgroundColor = .white
        
        addSubview(postImage)
        addSubview(label)
        addSubview(timeLabel)
        addSubview(publishButton)
        addConstraintsWithFormat("H:|-8-[v0]-8-[v1(35)]-8-|", views: postImage, timeLabel)
        addConstraintsWithFormat("H:|-8-[v0]-8-|", views: label)
        addConstraintsWithFormat("H:|-8-[v0]-8-|", views: publishButton)
        addConstraintsWithFormat("V:|-8-[v0]", views: timeLabel)
        addConstraintsWithFormat("V:|-8-[v0(150)]-8-[v1]-8-[v2]", views: postImage, label, publishButton)
        buttonBottomConnstraint = publishButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
        buttonBottomConnstraint?.isActive = true
        buttonHeightContraint = publishButton.heightAnchor.constraint(equalToConstant: 40)
        buttonHeightContraint?.isActive = true
        
    }
    
    func hideButton() {
        
        self.buttonHeightContraint?.constant = 0
        self.buttonBottomConnstraint?.constant = 0
        publishButton.setTitle("", for: .normal)
        self.backgroundColor = .lightGray
    }
    
    func showButton() {
        self.buttonHeightContraint?.constant = 40
        self.buttonBottomConnstraint?.constant = -8
        publishButton.setTitle("Share", for: .normal)
        self.backgroundColor = .white
    }
    
    func publishHandler() {
        self.delegate?.publishOnFacebook(post: post) { uploaded in
            
            if uploaded {
                DispatchQueue.main.async {
                    self.hideButton()
                    self.post?.setPosted()
                }
            }
        }
    }
}

