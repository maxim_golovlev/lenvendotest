//
//  CustomActivityIndicator.swift
//  LenvendoTest
//
//  Created by Admin on 21.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class CustomActivityIndicator: UIView {
    
    let boxView : UIView = {
        let boxView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        boxView.backgroundColor = .black
        boxView.alpha = 0.9
        boxView.layer.cornerRadius = 10
        return boxView
    }()
    
    let activityView: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        activityView.frame = CGRect(x: 20, y: 12, width: 40, height: 40)
        activityView.startAnimating()
        return activityView
    }()

    let textLabel: UILabel = {
        let textLabel = UILabel(frame: CGRect(x: 0, y: 50, width: 80, height: 30))
        textLabel.textColor = UIColor.white
        textLabel.textAlignment = .center
        textLabel.font = UIFont(name: textLabel.font.fontName, size: 13)
        textLabel.text = "Uploading..."
        return textLabel
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(boxView)
        boxView.center = self.center
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
